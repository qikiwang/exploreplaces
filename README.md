![alt text](https://bytebucket.org/qikiwang/exploreplaces/raw/bcbbce011e3c942a3c2ff9d8d26cbc22985f9ffd/exampleImg/example1.png "My project demo picture")

**This project will help people explore the place and find things they like.**



---

*This project is still in progress and currenlty has two parts.*

In the search page, the user could search the information near the place he/she wants to visit or look for the service around him/her. The data is provided by Google Places API.
![alt text](https://bytebucket.org/qikiwang/exploreplaces/raw/208785c4dcc9cf60a72de277358009f0be357939/exampleImg/example2.png "My project demo picture")
---


We also help local restuarants build their websites to help them be found by users more easily.
![alt text](https://bytebucket.org/qikiwang/exploreplaces/raw/208785c4dcc9cf60a72de277358009f0be357939/exampleImg/example3.png "My project demo picture")
